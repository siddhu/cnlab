import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
public class SendMail {
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
	System.out.println("Enter TO address: ");
        String to =  scanner.nextLine();
	System.out.println("To:"+to);
	System.out.println("Enter User id without @gmail part: ");
        String from = scanner.nextLine();
        System.out.println("From: "+from+"@gmail.com");
	final String password = "";                            //Set your Mail Password Here!
	final String host = "smtp.gmail.com";
        //Get System properties
	Properties props =  System.getProperties();
	// enable START TLS
	props.put("mail.smtp.starttls.enable","true");
	//setup mail server
	props.put("mail.smtp.host",host);
	//User
	props.put("mail.smtp.user",from);
	//Password
	props.put("mail.smtp.password",password);
	//Enable Authentication
	props.put("mail.smtp.auth","true");
	//TLS port
	props.put("mail.smtp.port","587");
	

        // Get the default Session object
        Session session = Session.getDefaultInstance(props);
        // compose the message
        try {
            // javax.mail.internet.MimeMessage class
            // is mostly used for abstraction.
            MimeMessage message = new MimeMessage(session);
            // header field of the header.
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(to));
	    System.out.println("Enter SUBJECT: ");
	    String subject = scanner.nextLine();
            message.setSubject(subject);
	    System.out.println("Enter TEXT:");
 	    String text =  scanner.nextLine();
            message.setText(text);
	    System.out.println("Authenticating.....");
	    System.out.println("Be patient while we send your mail!\n");
            // Send message
	    Transport transport = session.getTransport("smtp");
	    transport.connect(host,from,password);
            transport.sendMessage(message,message.getAllRecipients());
            System.out.println("Message Sent!\n");
	    transport.close();
        }
        catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }
}